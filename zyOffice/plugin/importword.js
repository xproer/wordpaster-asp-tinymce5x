(function () {
    'use strict';
    var global = tinymce.util.Tools.resolve('tinymce.PluginManager');
    function selectLocalImages(editor) {
      window.zyOffice.SetEditor(editor).api.openDoc()
    }

    var register$1 = function (editor) {
      this.getJsDir = function () {
          var js = document.scripts;
          var jsPath;
          for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("zyOffice/") > -1) {
              jsPath = js[i].src.substring(0, js[i].src.indexOf("zyOffice/"));
            }
          }
          return jsPath;
      };
      //http://localhost/WordPaster/css/
      var pathRes = this.getJsDir()+"zyOffice/plugin/";
      var ico = pathRes + "w.png";
      var img = '<img src='+ico+' />';

      editor.ui.registry.addButton('importword', {
        text: img,
        tooltip: '导入Word文档(docx)',
        onAction: function () {
          selectLocalImages(editor)
        }
      });
      editor.ui.registry.addMenuItem('importword', {
        text: img,
        tooltip: '导入Word文档(docx)',
        onAction: function () {
          selectLocalImages(editor)
        }
      });
    };
    var Buttons = { register: register$1 };
    function Plugin () {
      global.add('importword', function (editor) {        
        Buttons.register(editor);
      });
    }
    Plugin();
}());