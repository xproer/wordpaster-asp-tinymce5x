(function () {
    'use strict';
    var global = tinymce.util.Tools.resolve('tinymce.PluginManager');
    function selectLocalImages(editor) {        
        WordPaster.getInstance().SetEditor(editor);
        WordPaster.getInstance().importPPT()
    }

    var register$1 = function (editor) {
      this.getJsDir = function () {
          var js = document.scripts;
          var jsPath;
          for (var i = 0; i < js.length; i++) {
              if (js[i].src.indexOf("w.app.js") > -1) {
                  jsPath = js[i].src.substring(0, js[i].src.indexOf("w.app.js"));
              }
          }
          return jsPath;
      };
      //http://localhost/WordPaster/css/
      var pathRes = this.getJsDir() + "plugin/";
      var ico = pathRes + "ppt.png";
      var img = '<img src='+ico+' />';

      editor.ui.registry.addButton('pptimport', {
        text: img,
        tooltip: '导入PPT文档',
        onAction: function () {
          selectLocalImages(editor)
        }
      });
      editor.ui.registry.addMenuItem('pptimport', {
        text: img,
        tooltip: '导入PPT文档',
        onAction: function () {
          selectLocalImages(editor)
        }
      });
    };
    var Buttons = { register: register$1 };
    function Plugin () {
      global.add('pptimport', function (editor) {        
        Buttons.register(editor);
      });
    }
    Plugin();
}());