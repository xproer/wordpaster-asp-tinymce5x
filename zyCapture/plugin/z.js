(function () {
    'use strict';
    var global = tinymce.util.Tools.resolve('tinymce.PluginManager');
    function selectLocalImages(editor) {        
        window.zyCapture.setEditor(editor).Capture2();
    }

    var register$1 = function (editor) {
      this.getJsDir = function () {
          var js = document.scripts;
          var jsPath;
          for (var i = 0; i < js.length; i++) {
              if (js[i].src.indexOf("zyCapture/") > -1) {
                  jsPath = js[i].src.substring(0, js[i].src.indexOf("zyCapture/"));
              }
          }
          return jsPath;
      };
      //http://localhost/WordPaster/css/
      var pathRes = this.getJsDir() + "zyCapture/plugin/";
      var ico = pathRes + "z.png";
      var img = '<img src='+ico+' />';

      editor.ui.registry.addButton('zycapture', {
        text: img,
        tooltip: '截屏(zyCapture)',
        onAction: function () {
          selectLocalImages(editor)
        }
      });
      editor.ui.registry.addMenuItem('zycapture', {
        text: img,
        tooltip: '截屏(zyCapture)',
        onAction: function () {
          selectLocalImages(editor)
        }
      });
    };
    var Buttons = { register: register$1 };
    function Plugin () {
      global.add('zycapture', function (editor) {        
        Buttons.register(editor);
      });
    }
    Plugin();
}());